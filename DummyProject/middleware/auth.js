const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function(req, res, next) {
  // Getting token from header
  const token = req.header('key-variable');

  // Checking if token not exist
  if (!token) {
    return res
      .status(401)
      .json({ msg: 'There is no token, authorization denied' });
  }

  try {
    const decoded = jwt.verify(token, config.get('anshuSecret'));

    req.teacher = decoded.teacher;
    next();
  } catch (err) {
    res.status(401).json({ msg: 'Token is not valid' });
  }
};
