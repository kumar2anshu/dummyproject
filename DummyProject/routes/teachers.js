const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');

const Teacher = require('../models/Teacher');

// @route     POST api/teachers
// @desc      Regiter a teacher
// @access    Public
router.post(
  '/',
  [
    check('firstName', 'First Name is required ')
      .not()
      .isEmpty(),
    check('email', 'Please Input a valid Email').isEmail(),

    check('contact', 'Please Provide a valid mobile Number').isMobilePhone(),

    check('experience', 'please select your experience')
      .not()
      .isEmpty(),

    check('subject', 'Please select Your Subject')
      .not()
      .isEmpty(),

    check('location', 'Enter Your Location')
      .not()
      .isEmpty(),

    check(
      'password',
      'Enter a strong password with atleast 8 character'
    ).isLength({ min: 8 })
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      firstName,
      lastName,
      email,
      contact,
      experience,
      subject,
      location,
      password
    } = req.body;

    try {
      let teacher = await Teacher.findOne({ email });

      if (teacher) {
        return res.status(400).json({ msg: 'User already exists' });
      }

      teacher = new Teacher({
        firstName,
        lastName,
        email,
        contact,
        experience,
        subject,
        location,
        password
      });

      const salt = await bcrypt.genSalt(10);

      teacher.password = await bcrypt.hash(password, salt);

      await teacher.save();

      const payload = {
        teacher: {
          id: teacher.id
        }
      };

      jwt.sign(
        payload,
        config.get('anshuSecret'),
        {
          expiresIn: 3600
        },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
