const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { check, validationResult } = require('express-validator');
const Teacher = require('../models/Teacher');
const Course = require('../models/Course');

//@router GET api/courses
//@desc   Get all  courses uplodaed by  particular teacher
//@access Private

router.get('/', auth, async (req, res) => {
  try {
    const courses = await Course.find({ teacher: req.teacher.id }).sort({
      date: -1
    });
    res.json({ courses });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server from Error');
  }
});

//@router POST api/courses
//@desc  Add new Courses // teacher can add copurse
//@access Private

router.post(
  '/',
  [
    auth,
    [
      check('courseName', 'Name of the Course is Required')
        .not()
        .isEmpty(),
      check('courseDuration', ' Mention the course Duration')
        .not()
        .isEmpty(),
      check('mentor', 'Mentor Name is Required')
        .not()
        .isEmpty(),
      check(
        'courseStandard',
        'Menton the standard for which this course is design'
      )
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { courseName, courseDuration, mentor, courseStandard } = req.body;
    try {
      const newCourse = new Course({
        courseName,
        courseDuration,
        mentor,
        courseStandard,
        teacher: req.teacher.id
      });
      const course = await newCourse.save();
      res.json(course);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('server error');
    }
  }
);

//@route PUT api/courses/:id
//@desc  Update Coourse // teacher can update his course
//access Private

router.put('/:id', auth, async (req, res) => {
  const { courseName, courseDuration, mentor, courseStandard } = req.body;
  //building course object fotr update

  const myCourse = {};
  if (courseName) {
    myCourse.courseName = courseName;
  }
  if (courseDuration) {
    myCourse.courseDuration = courseDuration;
  }
  if (mentor) {
    myCourse.mentor = mentor;
  }
  if (courseStandard) {
    myCourse.courseStandard = courseStandard;
  }
  try {
    let course = await Course.findById(req.params.id);
    if (!course) {
      return res.status(404).json({ msg: 'This course is not found' });
    }
    //how to make sure that only that user can update their course??
    if (course.teacher.toString() !== req.teacher.id) {
      return res.status(401).json({ msg: 'You are not authorized to update' });
    }
    course = await Course.findByIdAndUpdate(req.params.id, { $set: myCourse });
    res.json(course);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Error from Server');
  }
});

//@router DELETE api/courses/:id
//@desc   Delete courses// teacher could only  delete his own course
//access  private
router.delete('/:id', auth, async (req, res) => {
  try {
    let course = await Course.findById(req.params.id);
    if (!course) {
      res.status(404).json({ msg: 'course with specified id not found' });
    }
    if (course.teacher.toString() !== req.teacher.id) {
      res
        .status(401)
        .json({ msg: 'You are not authorized to delete this course' });
    }
    await Course.findByIdAndRemove(req.params.id);
    res.json({ msg: 'course successfully removed' });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Error from server');
  }
});

module.exports = router;
