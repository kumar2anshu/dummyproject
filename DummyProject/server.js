const express = require('express');
const connectDataBase = require('./config/db');

const app = express();
//calling connectDataBase to connect
connectDataBase();

app.use(express.json({ extended: false }));
app.get('/', (req, res) =>
  res.send({ messsage: 'Welcome Anshu!! just Remember Coder Never Quit' })
);

//Define routes

app.use('/api/teachers', require('./routes/teachers'));
app.use('/api/courses', require('./routes/courses'));
app.use('/api/auth', require('./routes/auth'));

const PORT = process.env.port || 6000;
app.listen(PORT, () => {
  console.log(`Hi Anshu Server has started on port ${PORT} `);
});

