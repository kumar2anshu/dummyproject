const mongoose = require('mongoose');

const TeacherSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  subject: {
    type: String,
    required: true
  },
  contact: {
    type: String
  },
  experience: {
    type: String,
    required: true
  },
  location: {
    type: String
  },

  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('teacher', TeacherSchema);
