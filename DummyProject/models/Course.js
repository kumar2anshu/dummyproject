const mongoose = require('mongoose');

const CourseSchema = mongoose.Schema({
teacher:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"teachers" 

},

  courseName: {
    type: String,
    required: true
  },
  
  courseStandard: {
    type: String,
    required: true
  },
  mentor: {
    type: String
  },
  courseDuration: {
    type: String,
    required: true
  },

 
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('course', CourseSchema);
