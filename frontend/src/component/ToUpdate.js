import React, { Component } from 'react';
import axios from 'axios';
import { Table } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class ToUpdate extends Component {
  constructor(props) {
    super(props);
    //console.log(props.match.params.Id);

    this.state = {
      details: [],
      courseClass: '',
      courseName: '',
      courseDuration: '',
      mentor: '',
      cid: ''
    };
  }
  changeClassHandler = e => {
    this.setState({
      courseClass: e.target.value
    });
  };
  courseNameChangeHandler = e => {
    this.setState({
      courseName: e.target.value
    });
  };
  courseDurationChangeHandler = e => {
    this.setState({
      courseDuration: e.target.value
    });
  };
  changeMentorHandler = e => {
    this.setState({
      mentor: e.target.value
    });
  };

  handleUpdate = e => {
    e.preventDefault();

    const data = {
      courseClass: this.state.courseClass,
      courseName: this.state.courseName,
      courseDuration: this.state.courseDuration,
      mentor: this.state.mentor
    };

    //console.log(data);
    //console.log(this.props.match.params.Id);

    axios
      .put(
        `http://localhost:5000/api/finalUpdate/${this.props.match.params.Id}`,
        data
      )
      .then(res => {
        console.log(res);
        if (res.statusText === 'OK') {
          alert('Data successfully updated');
          // this.props.history.push('/courseavailaible');
        } else console.log('Some error');
      });
    window.location.reload();
  };
  componentDidMount() {
    let id = this.props.match.params.Id;

    axios.get(`http://localhost:5000/api/updateById/${id}`).then(res => {
      console.log(res);

      this.setState({
        details: res.data,
        cid: this.props.match.params.Id,
        mentor: this.state.details.mentor
      });
    });
  }

  render() {
    return (
      <div>
        <div>
          <center>
            <h3> Your previous data is as below:-</h3>
          </center>
          {<br />}
          <Table>
            <thead>
              <tr>
                <th>Serial No.</th>
                <th>Course Name</th>
                <th>Course Duration</th>
                <th>Standard</th>
                <th>Mentor Name</th>
                <th>Course Id</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope='row'>1</th>
                <td>{this.state.details.courseName}</td>
                <td>{`${this.state.details.courseDuration} Month`}</td>
                <td>{`Class (${this.state.details.courseClass})`} </td>
                <td>{this.state.details.mentor}</td>
                <td>{this.state.details._id}</td>
              </tr>
            </tbody>
          </Table>
        </div>

        <div>
          <center>
            <Form onSubmit={this.handleUpdate} className='courseForm'>
              <div>
                <center>
                  <h2>Update Your details</h2>
                </center>
              </div>
              <FormGroup>
                <Label htmlFor='courseClass'>courseClass* :</Label>
                <Input
                  type='Number'
                  placeholder='Choose the class for which this course is designed'
                  name='courseClass'
                  min='1'
                  max='12'
                  value={this.state.courseClass}
                  onChange={this.changeClassHandler}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor='courseName'>Course Name:</Label>
                <Input
                  type='text'
                  placeholder='Enter Your course name'
                  name='courseName'
                  value={this.state.courseName}
                  onChange={this.courseNameChangeHandler}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor='courseDuration'>
                  courseDuration (in month)*
                </Label>
                <Input
                  type='number'
                  placeholder='choose your course duration'
                  min='1'
                  max='6'
                  value={this.state.courseDuration}
                  onChange={this.courseDurationChangeHandler}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor='mentor'>Mentor Name:</Label>
                <Input
                  type='text'
                  placeholder='Enter Your Mentor Name'
                  value={this.state.mentor}
                  name='mentor'
                  onChange={this.changeMentorHandler}
                />
              </FormGroup>

              <Button type='submit'>Update</Button>
            </Form>
          </center>
        </div>
      </div>
    );
  }
}

export default ToUpdate;
