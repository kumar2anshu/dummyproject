import React, { Component } from 'react';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className='home'>
        <center>
          <h1>Welcome to FreeEducationAdda!!</h1>
        </center>
      </div>
    );
  }
}

export default Home;
