import React, { Component } from 'react';
// //import { Jumbotron } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import CourseCard from './CourseCard';

import axios from 'axios';

class CourseAvailaible extends Component {
  constructor(props) {
    super(props);

    this.state = {
      course: []
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = e => {
    e.preventDefault();
    axios.get('http://localhost:5000/api/courseavailaible').then(res => {
      console.log(res);
      this.setState({ course: res.data });
    });

    //console.log(this.state.courses);
  };
  render() {
    let courseDetails = this.state.course.map(course => {
      return (
        <Col>
          <CourseCard course={course} />
        </Col>
      );
    });
    return (
      <>
        <h1>Courses</h1>
        <button onClick={this.handleClick}>Available Courses</button>
        <Container fluid>
          <Row>{courseDetails}</Row>
        </Container>
      </>
    );
  }
}

export default CourseAvailaible;
