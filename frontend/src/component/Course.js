import React, { Component } from 'react';
//import { Jumbotron } from 'reactstrap';
import axios from 'axios';
//import '../App.css';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class Course extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courseClass: '',
      courseName: '',
      courseDuration: '',
      mentor: ''
    };
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = {
      courseClass: this.state.courseClass,
      courseName: this.state.courseName,
      courseDuration: this.state.courseDuration,
      mentor: this.state.mentor
    };

    axios.post('http://localhost:5000/api/courses', data).then(res => {
      console.log(res);
      if (res.statusText === 'OK') {
        alert('Thank You!! Your course is successfully added');
        this.props.history.push('/');
      }
    });
  };

  render() {
    const { courseClass, courseName, courseDuration, mentor } = this.state;
    return (
      <>
        <center>
          <Form onSubmit={this.handleSubmit} className='courseForm'>
            <div>
              <center>
                <h2>Enter Course Details</h2>
              </center>
            </div>
            <FormGroup>
              <Label htmlFor='courseClass'>courseClass* :</Label>
              <Input
                type='Number'
                placeholder='Choose the class for which this course is designed'
                name='courseClass'
                min='1'
                max='12'
                value={courseClass}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='courseName'>Course Name:</Label>
              <Input
                type='text'
                placeholder='Enter Your course name'
                name='courseName'
                value={courseName}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='courseDuration'>courseDuration (in month)*</Label>
              <Input
                type='number'
                placeholder='choose your course duration'
                name='courseDuration'
                min='1'
                max='6'
                value={courseDuration}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor='mentor'>Mentor Name:</Label>
              <Input
                type='text'
                placeholder='Enter Your Mentor Name'
                value={mentor}
                name='mentor'
                onChange={this.handleChange}
              />
            </FormGroup>
            {/* <FormGroup>
              <Label for='Article'>Article</Label>
              <Input type='file' name='article' id='article' />
              <FormText color='muted'>Upload Your Article here.</FormText>
            </FormGroup>
           */}
            <Button>ADD This Course</Button>
          </Form>
        </center>
      </>
    );
  }
}
export default Course;
