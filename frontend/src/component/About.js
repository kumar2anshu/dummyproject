import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  CardLink
} from 'reactstrap';
import '../App.css';
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from 'reactstrap';

const items = [
  {
    src: './images/img2.jpg',
    altText: 'Slide 1',
    caption: 'Slide 1'
  },
  { src: './images/img5.jpg', altText: 'Slide 2', caption: 'Slide 2' },
  { src: './images/img4.jpg', altText: 'Slide 3', caption: 'Slide 3' }
];

class About extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === items.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? items.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = items.map(item => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} />
          <CarouselCaption
            captionText={item.caption}
            captionHeader={item.caption}
          />
        </CarouselItem>
      );
    });

    return (
      <div>
        <div>
          <center>
            <h2>We are providing free and effective education service</h2>
          </center>
        </div>
        <div>
          <Carousel
            activeIndex={activeIndex}
            next={this.next}
            previous={this.previous}
          >
            <CarouselIndicators
              items={items}
              activeIndex={activeIndex}
              onClickHandler={this.goToIndex}
            />
            {slides}
            <CarouselControl
              direction='prev'
              directionText='Previous'
              onClickHandler={this.previous}
            />
            <CarouselControl
              direction='next'
              directionText='Next'
              onClickHandler={this.next}
            />
          </Carousel>
        </div>

        <div className='divli'>
          <h3>Our Sponsors</h3>

          <ul>
            <li>
              <a href='https://mhrd.gov.in/'>MHRD -Government of India</a>
            </li>
            <li>
              <a href='https://www.karnataka.com/govt/education-links/'>
                Education Department (Karnataka Government){' '}
              </a>
            </li>
            <li>
              <a href='http://www.iitb.ac.in/'>IIT Bombay</a>
            </li>
          </ul>
        </div>
        <div className='contactus'>
          <h3>Contact Us</h3>
          <ul>
            <li>
              FreeEducationAdda <br />
            </li>
            <li>
              SigmaInfoSolutions Ltd <br />
            </li>
            <li>Bengalore</li>
            <li>Karnataka</li>
            <li>Pin 560076</li>
          </ul>
        </div>
        <div>
          <Card className='developerImage'>
            <CardImg
              className='cardImage'
              src='./images/developerimage.jpg'
              alt='Card image cap'
            />
            <CardBody>
              <CardTitle>{<h1>Anshu Kumar</h1>}</CardTitle>
              <ul>
                <li>
                  <CardSubtitle>Developer</CardSubtitle>
                </li>
                <li>
                  <CardText>
                    FreeEducationAdda is an Interesting Project on which I am
                    working
                  </CardText>
                </li>
                <li>
                  <CardLink href='https://www.linkedin.com/in/anshu-kumar%E2%9C%94-756613153/'>
                    LinkedIn
                  </CardLink>
                </li>
              </ul>
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

export default About;
