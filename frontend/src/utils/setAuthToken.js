import axios from 'axios';

const setAuthToken = token => {
  if (token) {
    axios.defaults.headers.common['key-variable'] = token;
  } else {
    delete axios.defaults.headers.common['key-variable'];
  }
};

export default setAuthToken;
